
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
#from test_functions import *
from scipy.stats.stats import pearsonr 

def readImagePage(filepath):
	import cv2
	#img = cv2.imread(filepath,0)
	img = cv2.imread(filepath)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	
	
	return img
	
def expSmooth(series,alfa):
    result=[series[0]]
    for n in range(1,len(series)):
        result.append(alfa*series[n]+(1-alfa)*result[n-1])
    return result
	
def createPadding(img, margin):
	import numpy as np
	w=img.shape[1]
    #print(w)
	for i in range(margin):
		img = np.insert(img, w, values=255, axis=1)

	for i in range(margin):
		img = np.insert(img, 0, values=255, axis=1)    
    
	h=img.shape[0]
    #print(w)
	for i in range(margin):
		img = np.insert(img, h, values=255, axis=0)
    
	for i in range(margin):
		img = np.insert(img, 0, values=255, axis=0)
    
    
	return img

def erodeCircly(word_img,radius,padding):
    import cv2
    word_img=createPadding(word_img, padding) 
    kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(radius,radius))
    erode_img = cv2.erode(word_img, kernel, iterations=1)
    return erode_img

def xxx_crop_image(img,tol=0):
    # img is 2D image data
    # tol  is tolerance
    mask = img>tol
    print(mask)
    print(np.ix_(mask.any(1),mask.any(0)))
    return img[np.ix_(mask.any(1),mask.any(0))]



def cropImage2(img,margin):
	#print(img.shape)
	img=cv2.bitwise_not(img)
	
	right_left=np.sum(img, axis = 0)
	up_down=np.sum(img, axis = 1)

	#print(right_left)

	#print(right_left.size)
	#print(right_left.shape[0])
	pixels_width=right_left.shape[0]
	pixels_height=up_down.shape[0]
	
	
	#print(pixels_width,pixels_height)

	flag_left=False
	flag_right=False

	for left_x in range(0, pixels_width):	
		#print('left_x')
		if right_left[left_x]>0 and flag_left==False:
			#print(left_x)
			#flag_left=True
			break
			
	for right_x in range(0, pixels_width):
		if right_left[pixels_width-1-right_x]>0 and flag_right==False:
			#print(right_x)
			#flag_right=True
			break
	##################################
	flag_up=False
	flag_down=False
	for up_y in range(0, pixels_height):
		#print(i)
		if up_down[up_y]>0 and flag_up==False:
			#print(up_y)
			#flag_left=True
			break
			
	for down_y in range(0, pixels_height):
		#print(i)
		if up_down[ pixels_height-1-down_y]>0 and flag_down==False:
			#print(down_y)
			#flag_left=True
			break
	#print(left_x)
	#print(right_x)
	#print(up_y)
	#print(down_y)	
	#margin=10
	return (left_x-margin,up_y-margin),(pixels_width-0-right_x+margin,pixels_height-0-down_y+margin)	

def  strechImages2(img1,img2):
    import cv2
    #print(img1.shape,img2.shape)
    w=max(img1.shape[0],img2.shape[0])
    h=max(img1.shape[1],img2.shape[1])
    
    
    #print('h',h,'w',w)
    
    img1 = cv2.resize(img1,(int(h),int(w)))
    img2 = cv2.resize(img2,(int(h),int(w)))
    return img1,img2

def findLowerProfile2(img3):
	import cv2
	import numpy as np
	img3 = cv2.flip( img3, 1 )
	img3 = cv2.flip( img3, -1 )
	img3= cv2.bitwise_not(img3).copy()
	mtrx=[]
	for col in img3.T:
        #print(col)
		count=0
		for row in col:     
            #print(row)
			if row==0:
				count=count+1
                #print(count)
			else: 
				break
        #print('count=',count)
		mtrx.append(count)
	mtrx=np.asarray(mtrx)
	h=img3.shape[1]
	mtrx=h-mtrx
	mtrx = mtrx/max(mtrx)
    #mtrx = mtrx/max(mtrx)
	return mtrx



def findUpperProfile2(img3):
	import cv2
	import numpy as np
	img3= cv2.bitwise_not(img3).copy()
	mtrx=[]
	for col in img3.T:
        #print(col)
		count=0
		for row in col:       
            #print(row)
			if row==0:
				count=count+1
                #print(count)
			else: 
				break
        #print('count=',count)
		mtrx.append(count)
	mtrx=np.asarray(mtrx)
	h=img3.shape[1]
	mtrx=h-mtrx
	mtrx = mtrx/max(mtrx)
    #mtrx = mtrx/max(mtrx)
	return mtrx
		
def clearBorderBox2(img):
	import cv2
	w=img.shape[1]
	h=img.shape[0]
	
	#plt.figure()
	#plt.imshow(img,cmap='gray')
	#plt.show()
	
	if w==0 or h==0:
		print(w,h,'-----')
    
    
    
    
    
    
	for row in range(h):
		if img[row, 0] == 0:
			#print(row)
			cv2.floodFill(img, None, (0, row), 255)
		if img[row, w-1] == 0:
			cv2.floodFill(img, None, (w-1, row), 255)
    
	for col in range(w):
		if img[0, col] == 0:
			cv2.floodFill(img, None, (col, 0), 255)
		if img[h-1, col] == 0:
			cv2.floodFill(img, None, (col, h-1), 255)
	return img		


def cropTotal(query):
	
	
	
	#pixels_width=query.shape[0]
	#pixels_height=query.shape[1]
	#print(pixels_width,pixels_height,'++++++++++++')
	 
	#lu,rd=cropImage2(query,margin=0)#left-upper point and right-down point
	(xa,ya),(xb,yb)=cropImage2(query,margin=0)#left-upper point and right-down point
	
	#query=query[lu[1]:rd[1],lu[0]:rd[0]]
	query=query[ya:yb,xa:xb]
	#print(lu)
	#print(rd)
	
	return query, ((xa,ya),(xb,yb))



def erodeHorizontally2(pageImgBin,hkernelsize):
	import cv2
	import numpy as np
	kernel = np.ones((1,hkernelsize), np.uint8)
	pageImgBinEroded = cv2.erode(pageImgBin, kernel, iterations=1)
	return pageImgBinEroded 
	
def findBoxes_Enlarge2(pageImgBin,areaMin,areaMax,enlarge,hkernelsize):
	import cv2
	pageImgBinCopy=pageImgBin.copy()# make copy in order not to touch accidently pageImgBin

	#.copy()
	#imgOr=pageImgBin
	pageImgBinEroded=erodeHorizontally2(pageImgBin,hkernelsize)
	
	heights=[]
	croppedImgs=[]
	boxes=[]
	areas=[]
	(major, minor, _) = cv2.__version__.split(".")
	#print(major)
	if major=="3":
		_, contours, hierarchy = cv2.findContours(pageImgBinEroded,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE) #opencv 3.4
	if major=="4":
		contours, hierarchy = cv2.findContours(pageImgBinEroded,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)  #4.2
	#print(len(contours))
	for (i, c) in enumerate(contours):
		area = cv2.contourArea(c)
		#print(area)
		#areas.append(area)
		if area < areaMin or area > areaMax:
		# do not process small or large contours
			continue
        
        
		
		#if w==0 or h==0:
		#	
		
		#rects.append(c)
		(x, y, w, h) = cv2.boundingRect(c)
		#print(x, y, w, h)
		 
		
		########################################
		########################################
		if y-enlarge<0: #check for negative values of the following cropped image
			#print(y-enlarge,y+h,'-----')
			enlarge=y	
			#continue
		
		croppedImg = pageImgBinCopy[y-enlarge:y+h, x:x+w].copy()
		#w=croppedImg.shape[1]
		#h=croppedImg.shape[0]
		#if w==0 or h==0:
			#print(x, y,w,h,'$$$$$$$')
			
			
		########################################
		########################################
		
		croppedImgs.append(croppedImg)
		#print(h)# find average of h as enlarge
		heights.append(h)
		#coords.append((x, y-enlarge, w, h+enlarge))
				#boxes.append((x, y-enlarge, x+w, y+h))
		
		boxes.append(((x, y-enlarge), (x+w, y+h)))
		
        #rect_img=cv2.rectangle(source_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
	
	boxAvgHeight=np.mean(heights, axis = 0)
	#print(np.mean(areas, axis = 0))
	#print(boxAvgHeight)
	return croppedImgs,boxes,boxAvgHeight
	



def segmentPage(pageImgBin,threshold,areaMin,areaMax,enlarge,hkernelsize):
	#finalImg=pageImgBin

	#_,_,boxAvgHeight=findBoxes_Enlarge2(pageImgBin,areaMin=1200,areaMax=30000,enlarge=1,hkernelsize=11)
	#enlargeNum=np.ceil(boxAvgHeight/2).astype(int) 
	#print(enlargeNum)
	#enlargeNum=17
	#areaMin=600
	#areaMax=30000
	#enlarge=enlargeNum
	#hkernelsize=11
	#print(enlargeNum)
	#croppedImgs,boxes,boxAvgHeight=findBoxes_Enlarge2(pageImgBin,areaMin=600,areaMax=30000,enlarge=enlargeNum,hkernelsize=11)
	croppedImgs,boxes,boxAvgHeight=findBoxes_Enlarge2(pageImgBin,areaMin,areaMax,enlarge,hkernelsize)
	
	# make coorrection to the upper croppedImgs, so as the function to return only boxes and then crop the imgs
	count=-1
	newBoxes=[]
	for img in croppedImgs:
		count=count+1
		
		
		 
		##########################
		##w=img.shape[1]
		##h=img.shape[0]
		##if w==0 or h==0:
			#print(w,h,'-----')
			##continue
		
		img1=clearBorderBox2(img)
				
		img2=cutPunctuation2(img1, threshold)
		
		#img2=img1
				#make this function
				#examine the routine
		if img2.shape[0]==0 or img2.shape[1]==0: # perispomeni me tono eikona
			#print(img1.shape,'-----')
			#print(img2.shape,'==========')
			#plt.figure()
			#plt.imshow(img1,cmap='gray')
			#plt.show()
			continue
			
		if img2 is None:
			continue
			#print(img1.shape[0],img1.shape[1],"0000000000000")
			#plt.figure()
			#plt.imshow(img,cmap='gray')
			#plt.show()
			#sys.exit()
		##########################
		
		xaf=boxes[count][0][0]
		yaf=boxes[count][0][1]
		#xbf=boxes[count][1][0]
		ybf=boxes[count][1][1]
		
			#sys.exit()
				
		_,((dxa,dya),(dxb,dyb))=cropTotal(img2)

		start_point = (xaf+dxa,  yaf+dya)
		end_point = (xaf+dxb,  ybf)
		
		newBoxes.append(((xaf+dxa, yaf+dya), (xaf+dxb, ybf)))
		
		#color = (0, 255, 0) 
		#thickness = 2
		#coords.append((x, y-enlarge, x+w, y+h))
		#finalImg= cv2.rectangle(finalImg , start_point, end_point, color, thickness) 	
		#cv2.imwrite('test/'+str(count)+'.png',img)



	#cv2.imwrite('test/'+'page.png',finalImg)
	return newBoxes
	
def cutPunctuation2(img, threshold):
	
	import cv2
	import numpy as np
	img = cv2.bitwise_not(img) 
	img=img//255
	y=img.sum(axis=0)
	xindex=np.where(y == 0)[0]
	numxs=xindex.shape[0]
	rev_xindex=xindex[::-1]
	#print(rev_xindex)
	#print(numxs,'ooooooooooo')
	width=np.size(img,1) 
	height=np.size(img,0)
	
	#print(img.shape[0])
	
	#for i in  range(0, numxs-1):
	for i in  range(numxs-1):
		#print(rev_xindex[i+1] ,rev_xindex[i])
		if rev_xindex[i+1]<rev_xindex[i]-1: #by pass the contigues zeros that exist 
			xc=rev_xindex[i+1] 
			
			imgF = img[0:height, 0:xc]    
			imgT= img[0:height, xc:width]
			half=height/2
			hprof=imgT.sum(axis=1)
			sum_area=hprof.sum()
			#print(xc)  
			#print( sum_area,threshold)
			
			if sum_area <=threshold:      
                #cut      
				imgF=255*imgF
				imgF=abs(255-imgF)
				imgReturn=imgF
                #plt.figure()
                #plt.imshow(imgF)
				#print('111')
				return imgReturn
				#return imgReturn
			else:
				#print('222')
                #do not cut
				img=255*img
				img=abs(255-img)
				imgReturn=img
				return imgReturn
		
	#print('333')
	img=255*img
	img=abs(255-img)
	imgReturn=img
		
	#plt.figure()
	#plt.imshow(img,cmap='gray')
	#plt.show()
		
	return imgReturn
	
	
	
	
def xxxxxxcutPunctuation2(img, threshold):
	
	import cv2
	import numpy as np
	img = cv2.bitwise_not(img) 
	img=img//255
	y=img.sum(axis=0)
	xindex=np.where(y == 0)[0]
	numxs=xindex.shape[0]
	rev_xindex=xindex[::-1]
	#print(rev_xindex)
	#print(numxs,'ooooooooooo')
	width=np.size(img,1) 
	height=np.size(img,0)
	
	#print(img.shape[0])
	
	#for i in  range(0, numxs-1):
	for i in  range(numxs-1):
		
		if rev_xindex[i+1]<rev_xindex[i]-1: #by pass the contigues zeros that exist 
			xc=rev_xindex[i+1] 
			
			imgF = img[0:height, 0:xc]    
			imgT= img[0:height, xc:width]
			half=height/2
			hprof=imgT.sum(axis=1)
			sum_area=hprof.sum()
			print(xc)  
			print( sum_area,threshold)
			
			if sum_area <=threshold:      
                #cut      
				imgF=255*imgF
				imgF=abs(255-imgF)
				imgReturn=imgF
                #plt.figure()
                #plt.imshow(imgF)
				print('111')
				return imgReturn
				#return imgReturn
			else:
				print('222')
                #do not cut
				img=255*img
				img=abs(255-img)
				imgReturn=img
				return imgReturn
		
		print('333')
		img=255*img
		img=abs(255-img)
		imgReturn=img
		
		#plt.figure()
		#plt.imshow(img,cmap='gray')
		#plt.show()
		
		return imgReturn
				
			

def xxxcutPunctuation2(img, threshold):
	import cv2
	import numpy as np
	img = cv2.bitwise_not(img) 
	img=img//255
	y=img.sum(axis=0)
	xindex=np.where(y == 0)[0]
	numxs=xindex.shape[0]
	rev_xindex=xindex[::-1]
	print(rev_xindex)
	width=np.size(img,1) 
	height=np.size(img,0)
	for i in  range(0, numxs-1):
		if rev_xindex[i+1]<rev_xindex[i]-1: #by pass the contigues zeros that exist 
			xc=rev_xindex[i+1]   
			imgF = img[0:height, 0:xc]    
			imgT= img[0:height, xc:width]
			half=height/2
			hprof=imgT.sum(axis=1)
			sum_area=hprof.sum()
			if sum_area <=threshold:      
                #cut      
				imgF=255*imgF
				imgF=abs(255-imgF)
				imgReturn=imgF
                #plt.figure()
                #plt.imshow(imgF)
				return imgReturn
			else:
                #do not cut
				img=255*img
				img=abs(255-img)
				imgReturn=img
				return imgReturn

def xxxold_cutPunctuation2(img, threshold):
	import cv2
	import numpy as np
    #plt.imshow(img)
	img = cv2.bitwise_not(img) 
	newX=256
	newY=256
    #plt.figure()
    #plt.imshow(img)
    #img = cv2.resize(img,(int(newX),int(newY)))
	img=img//255
	y=img.sum(axis=0)
	num=np.size(img,1)
	x = np.arange(num)
    ###print(x.shape)
    ###fig, ax = plt.subplots()
    ###ax.plot(x,y)
    ###plt.show()
    ###plt.figure()
    
    

    
	index=np.where(y == 0)[0]
	len1=index.shape[0]
	rev_index=index[::-1]

    
   
    
	width=np.size(img,1) 
	height=np.size(img,0)

    
    
	sum_ink=0
	for i in  range(0, len1-1):
        #print(i,"++++")
        
		if rev_index[i+1]<rev_index[i]-1: #by pass the contigues zeros that exist
            #print("dif=",rev_index[i]-rev_index[i+1],"index=",i )
            #print(rev_index[i])
            #print(rev_index[i+1])
            
			xc=rev_index[i+1]
            
			imgF = img[0:height, 0:xc]
            
            
			imgT= img[0:height, xc:width]
			half=height/2
            #print('==========')
            #print('half',half)
            #print('xc',xc)
			hprof=imgT.sum(axis=1)
			sumakiarea=hprof.sum()
            #hprofIndex=np.where(hprof == 0)
            #hprofZerosNum=np.size(hprofIndex,1)
            
            #print('zeros',hprofZerosNum)
            
            #print('==========')
            #if hprofZerosNum-3 >= half:
			if sumakiarea <=threshold: 
                
                #cut
                
				imgF=255*imgF
				imgF=abs(255-imgF)
				imgReturn=imgF
                
                #plt.figure()
                #plt.imshow(imgF)
				return imgReturn
			else:
                #do not cut
				img=255*img
				img=abs(255-img)
				imgReturn=img
				return imgReturn


def drawBoxesOnPage(pageImgBin,threshold,areaMin,areaMax,enlarge,hkernelsize):
	newBoxes=segmentPage(pageImgBin,threshold,areaMin,areaMax,enlarge,hkernelsize)
	finalImg=pageImgBin.copy()
	for pntPair in newBoxes:
		color = (0, 255, 0)
		thickness = 1
		#print(pntPair) 
		finalImg= cv2.rectangle(finalImg ,pntPair[0], pntPair[1], color, thickness) 
	#cv2.imwrite('test/'+'page2.png',finalImg)
	#plt.figure()
	#plt.imshow(pageImgBin,cmap='gray')
	#plt.show()
	return 	finalImg,newBoxes
	
def xxxinit1():
	import cv2
	import matplotlib.pyplot as plt
	filepath="135half.png"
	filepath="0151.pdf43196.png"
	filepath='0151.pdf3001.png'
	pageImg=readImagePage(filepath)
	(thresh, pageImgBin) = cv2.threshold(pageImg, 10, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

	plt.figure()
	plt.imshow(pageImgBin,cmap='gray')
	plt.show()
	
	
	# find height-> (space between words>height) -> horizontal kernel -> areaMin -> enlarge
	# h=40 -> kernel=h/4 -> areaMin=h*h/4 -> enlarge=h/2
	import cv2
	import matplotlib.pyplot as plt
	from scipy.interpolate import interp1d
	img = cv2.bitwise_not(pageImgBin) 
	img=img//255
	
	hprof=img.sum(axis=1)
	x = np.arange(hprof.size)
	print(hprof.size)
	
	xnew = np.linspace(0, hprof.size-1, num=hprof.size, endpoint=True)
	f = interp1d(x, hprof, kind='cubic')
	
	
	
	
	#print(hprof.size)
	#num=np.size(hprof,1)
	#print(num)
	##################
	
	
	#test_xindex=np.where(np.logical_and(hprof>=200, hprof<=210))[0]
	#test_xindex=np.where(np.logical_and(f(xnew)>=200, f(xnew)<=210))[0]
	xindexArr=np.where( f(xnew)>300 )[0]
	for i in range(xindexArr.size-1):
		
		if xindexArr[i]+1<xindexArr[i+1]:
			print(xindexArr[i],xindexArr[i+1],xindexArr[i+1]-xindexArr[i])
	
	
	
	print(xindexArr)
	
	plt.plot(x, hprof, 'o', xnew, f(xnew), '-')
	plt.show()
	
	###################
	return
	
	
	
	
	
	
	
	
	
	fig, ax = plt.subplots()
	ax.plot(x,hprof)
	#ax.plot(x,hprof/2)
	
	plt.show()
	
	#y = np.arange(1000)
	#ax.plot(300,y)
	
	#plt.figure()
	return
	
	xindex=np.where(hprof > 100)[0]
	
	print(xindex)
	 
	#newX=256
	#newY=256
    #plt.figure()
    #plt.imshow(img)
    #img = cv2.resize(img,(int(newX),int(newY)))
	
	#sumakiarea=hprof.sum()
	
	#plt.figure()
	#plt.imshow(pageImgBinEroded,cmap='gray')
	#plt.show()
	return
	
	#,areaMin,areaMax,enlarge,hkernelsize
	import cv2
	pageImgBinCopy=pageImgBin.copy()
	#imgOr=pageImgBin
	hkernelsize=100
	pageImgBinEroded=erodeHorizontally2(pageImgBin,hkernelsize)
	plt.figure()
	plt.imshow(pageImgBinEroded,cmap='gray')
	plt.show()
	
	heights=[]
	croppedImgs=[]
	boxes=[]
	areas=[]
	(major, minor, _) = cv2.__version__.split(".")
	#print(major)
	if major=="3":
		_, contours, hierarchy = cv2.findContours(pageImgBinEroded,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE) #opencv 3.4
	if major=="4":
		contours, hierarchy = cv2.findContours(pageImgBinEroded,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)  #4.2
	#print(len(contours))
	for (i, c) in enumerate(contours):
		area = cv2.contourArea(c)
		#print(area)
		#areas.append(area)
		#if area < areaMin or area > areaMax:
		# do not process small or large contours
			#continue
		enlarge=0
		#rects.append(c)
		(x, y, w, h) = cv2.boundingRect(c)
		#croppedImg = pageImgBinCopy[y-enlarge:y+h, x:x+w]
		#croppedImgs.append(croppedImg)
		#print(h)# find average of h as enlarge
		heights.append(h)
		#coords.append((x, y-enlarge, w, h+enlarge))
				#boxes.append((x, y-enlarge, x+w, y+h))
		
		boxes.append(((x, y-enlarge), (x+w, y+h)))
		
        #rect_img=cv2.rectangle(source_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
	
	boxAvgHeight=np.mean(heights, axis = 0)
	#print(np.mean(areas, axis = 0))
	print(boxAvgHeight)
	print(heights)
	return croppedImgs,boxes,boxAvgHeight	


def piecewise_linear(x, x0, y0, k1, k2):
    return np.piecewise(x, [x < x0], [lambda x:k1*x + y0-k1*x0, lambda x:k2*x + y0-k2*x0])


def getInitValuesForPage(pageImgBin):
	import cv2
	import matplotlib.pyplot as plt
	from scipy.optimize import curve_fit
	import numpy as np
	
	listaHeight=[]	
	listaHeightMeans=[]
	listaWidth=[]
	listaWidthMeans=[]
	
	starthkernel=1
	maxhkernel=42
	
	for k in range(starthkernel,maxhkernel):
		_,boxes,_=findBoxes_Enlarge2(pageImgBin,areaMin=0,areaMax=pageImgBin.shape[0],enlarge=0,hkernelsize=k)
		# i find the height of the characters or block of characters so the kernel is neraly the same.
		
		
		for (i, c) in enumerate(boxes):
			xaf=boxes[i][0][0]
			yaf=boxes[i][0][1]
			xbf=boxes[i][1][0]
			ybf=boxes[i][1][1]
			w=xbf-xaf
			h=ybf-yaf
			
			if h==pageImgBin.shape[0]: # throw out the page height
				#print(h)
				continue
				
			listaHeight.append(h)
			listaWidth.append(w)
		#print(lista)
		#print(k,'==========')
		#print('======================')
		listaHeightMeans.append(np.mean(listaHeight, axis = 0))
		listaWidthMeans.append(np.mean(listaWidth, axis = 0))
		
		#lista=listaWidth
		#listaMeans=listaWidthMeans
		
		lista=listaHeight
		listaMeans=listaHeightMeans
		#print(np.mean(lista, axis = 0)) 
		#print(np.std(lista))
		#print(np.var(lista)) 
		#print('') 
		
		#hist,bins = np.histogram(lista) 
		#print( hist )
		#print (bins) 
		#plt.hist(lista)
		#plt.title("histogram") 
		#plt.show()
	
	###############
	from scipy.signal import savgol_filter
	import pwlf
	x = np.linspace(starthkernel, maxhkernel, len(listaMeans))
	plt.plot(x,listaMeans)	
	plt.show()
	###############
	my_pwlf = pwlf.PiecewiseLinFit(x, listaMeans)
	breaks = my_pwlf.fit(2)
	print(breaks)
	x_hat = np.linspace(x.min(), x.max(), 100)
	y_hat = my_pwlf.predict(x_hat)
	plt.figure()
	plt.plot(x, listaMeans, 'o')
	plt.plot(x_hat, y_hat, '-')
	plt.show()
	###############
	
	y_hat = savgol_filter(listaMeans, 41, 3)
	x_hat = np.linspace(x.min(), x.max(), y_hat.size)
	my_pwlf = pwlf.PiecewiseLinFit(x_hat, y_hat)
	breaks = my_pwlf.fit(2)
	print(breaks)
	plt.plot(x_hat, y_hat, '-')
	plt.show()
	
	###############
	
	
	
	
	#print(x)
	#print(listaMeans)
	
	
	#print('===========')
	#print('estimated value= ',np.argmin(listaMeans)+1)
		#hkernelsize=np.argmin(listaMeans)+1
	#print(hkernelsize,'=========')
		#wordHeight=listaMeans[hkernelsize-1]
	wordHeight=np.mean(listaMeans, axis = 0) 
	
	hkernelsize=np.ceil(wordHeight*3/3 ).astype(int)
	enlarge=np.ceil(wordHeight*8/10).astype(int) 
	areaMin=(wordHeight)*(wordHeight)/4			# less than 1 of char area
	areaMax=(wordHeight*wordHeight)*200 		# more than 200 chars area
	threshold=(wordHeight)*(wordHeight)/2		# less than 1/2 of char area
	
	return wordHeight,threshold,areaMin,areaMax,enlarge,hkernelsize
	 		
		
def maina(filepath):
	#########################################
	#patrologia
	#areaMin=600
	#hkernelsize=11
	#dorotheos
	#areaMin=1200
	#hkernelsize=19
	#########################################
	#filepath="query.png"
	#filepath="0151.pdf3001.png"

###################################################
#
# THE SEGMENTATION PHASE RETURNS THE BOXES OF WORDS
#
###################################################
	
	
	#filepath='ok.png'
	#filepath="kairo.png"
	#filepath="5.png"
	
	
	#filepath="gentleman10.png"
	#filepath="gentleman11.png"
	#filepath="135.png"
	
	#filepath="test/hello-final.png"
	#filepath="test/2.png"
	print(filepath,'==')
	pageImg=readImagePage(filepath)
	print('Image is read<br>')
	(thresh, pageImgBin) = cv2.threshold(pageImg, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
	pageImgBinCopy=pageImgBin.copy()
	print('Image is binarized<br>')
	
	wordHeight,threshold,areaMin,areaMax,enlarge,hkernelsize=getInitValuesForPage(pageImgBinCopy)
	
	#print(wordHeight,kernelSize,enlarge,areaMin,areaMax)
	print('Image init values are estimated<br>------------------------<br>')
	print('wordHeight=',wordHeight,'<br>',
	'threshold=', threshold,'<br>',
	'kernelSize=',hkernelsize,'<br>',
	'enlarge=',enlarge,'<br>',
	'areaMin=',areaMin,'<br>',
	'areaMax=', areaMax,'<br>')
	
	
	
	print( '-----------------')
	#enlarge=20 # this makes the problem in findBoxes_Enlarge function
	#areaMax=400000		#discard big words
	#hkernelsize=14		#eroding horizontal
	#areaMin=440		# discard small words
	#enlarge=7			#for segment the word with accents
	#threshold=100		#for cutting commas
	finalImg,newBoxes=drawBoxesOnPage(pageImgBinCopy,threshold,areaMin,areaMax,enlarge,hkernelsize)
	
	
	#write json
	jsonB1='var arr={\n'
	jsonB2='{\n'
	jsonE='}'
	json=''
	count=-1
	for pntPair in newBoxes:
		count=count+1
		#color = (0, 255, 0)
		#thickness = 1
		#print(pntPair) 
		#finalImg= cv2.rectangle(finalImg ,pntPair[0], pntPair[1], color, thickness)
		x1=str(pntPair[0][0])+","
		y1=str(pntPair[0][1])+","
		x2=str(pntPair[1][0])+","
		y2=str(pntPair[1][1])+""
		
		
		json=json+"\"point"+str(count)+"\""+":\""+x1+y1+x2+y2+"\",\n"
	json=json+"\"point"+str(count)+"\""+":\""+x1+y1+x2+y2+"\"\n"
	
	json1=jsonB1+json+jsonE
		
	f=open('boxes.js','w')
	#s1='\n'.join(pntPair)
	f.write(json1)
	f.close()
	
	json2=jsonB2+json+jsonE
		
	f=open('boxes.json','w')
	#s1='\n'.join(pntPair)
	f.write(json2)
	f.close()
	
	
	
	
	from random import randint
	
	randomaki=str(randint(0, 100))
	print(randomaki)
	cv2.imwrite('test/'+'boxes'+randomaki+'.png',finalImg)
	print('------------------------<br>')
	print('Image is segmented<br>')
	#print('<a href="https://ocr.graeca.org/main_new/test/boxes.png">Show the Segmented Page!</a><br>')
	
	print('<a href="http://195.201.141.240:2015/phd/apix/ocrLab/main_new/load.html?'+randomaki+'&'+filepath+'">Show the Segmented Page!</a><br>')
	
	print('<img src="http://195.201.141.240:2015/phd/apix/ocrLab/main_new/test/boxes'+randomaki+'.png" alt="Smiley face" height="auto" width="42">')
	#plt.figure()
	#plt.imshow(pageImgBinCopy,cmap='gray')
	#plt.show()
	
	 
	

 
def test():
	print('none')
	return		


#maina(filepath)

 
